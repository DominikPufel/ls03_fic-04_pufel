package form_aendern;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Frame;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Frame_form_aendern extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField tfdtexteingeben;
	private JLabel lbltextaendern;
	private JPanel formaendernPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Frame_form_aendern frame = new Frame_form_aendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Frame_form_aendern() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 386, 586);
		formaendernPane = new JPanel();
		formaendernPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(formaendernPane);
		formaendernPane.setLayout(null);
		
		JButton btnrot = new JButton("Rot");
		btnrot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			formaendernPane.setBackground(Color.RED);
			}
		});
		btnrot.setBounds(10, 120, 113, 23);
		formaendernPane.add(btnrot);
		
		JButton btngruen = new JButton("Gr\u00FCn");
		btngruen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formaendernPane.setBackground(Color.GREEN);
			}
		});
		btngruen.setBounds(128, 120, 113, 23);
		formaendernPane.add(btngruen);
		
		JButton btnblau = new JButton("Blau");
		btnblau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formaendernPane.setBackground(Color.BLUE);
			}
		});
		btnblau.setBounds(247, 120, 113, 23);
		formaendernPane.add(btnblau);
		
		JButton btngelb = new JButton("Gelb");
		btngelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formaendernPane.setBackground(Color.YELLOW);
			}
		});
		btngelb.setBounds(10, 147, 113, 23);
		formaendernPane.add(btngelb);
		
		JButton btnstandart = new JButton("Standartfarbe");
		btnstandart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				formaendernPane.setBackground(Color.BLACK);
			}
		});
		btnstandart.setBounds(128, 147, 113, 23);
		formaendernPane.add(btnstandart);
		
		JButton btnfarbewaehlen = new JButton("Farbe w\u00E4hlen");
		btnfarbewaehlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		btnfarbewaehlen.setBounds(247, 147, 113, 23);
		formaendernPane.add(btnfarbewaehlen);
		
		JButton btnexit = new JButton("EXIT");
		btnexit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		btnexit.setBounds(10, 494, 360, 50);
		formaendernPane.add(btnexit);
		
		JButton btnarial = new JButton("Arial");
		btnarial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lbltextaendern.setFont(new Font("ARIAL", Font.PLAIN, lbltextaendern.getFont().getSize()));
			}
		});
		btnarial.setBounds(10, 212, 113, 23);
		formaendernPane.add(btnarial);
		
		JButton btncomic = new JButton("Comic Sans MS");
		btncomic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lbltextaendern.setFont(new Font("Comic Sans MS", Font.PLAIN, lbltextaendern.getFont().getSize()));
			}
		});
		btncomic.setBounds(128, 212, 113, 23);
		formaendernPane.add(btncomic);
		
		JButton btncourier = new JButton("Courier New");
		btncourier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lbltextaendern.setFont(new Font("Courier New", Font.PLAIN, lbltextaendern.getFont().getSize()));
			}
		});
		btncourier.setBounds(247, 212, 113, 23);
		formaendernPane.add(btncourier);
		
		tfdtexteingeben = new JTextField();
		tfdtexteingeben.setText("Hier bitte Text eingeben");
		tfdtexteingeben.setToolTipText("Hier bitte Text eingeben");
		tfdtexteingeben.setBounds(10, 240, 350, 20);
		formaendernPane.add(tfdtexteingeben);
		tfdtexteingeben.setColumns(10);
		
		JButton btnwritelabel = new JButton("Ins Label schreiben");
		btnwritelabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lbltextaendern.setText(tfdtexteingeben.getText());
			}
		});
		btnwritelabel.setBounds(10, 266, 170, 23);
		formaendernPane.add(btnwritelabel);
		
		JButton btndeletelabel = new JButton("Text im Label l\u00F6schen");
		btndeletelabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lbltextaendern.setText("");
			}
		});
		btndeletelabel.setBounds(190, 266, 170, 23);
		formaendernPane.add(btndeletelabel);
		
		JButton btnschriftrot = new JButton("Rot");
		btnschriftrot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lbltextaendern.setForeground(Color.RED);
			}
		});
		btnschriftrot.setBounds(11, 312, 113, 23);
		formaendernPane.add(btnschriftrot);
		
		JButton btnschriftblau = new JButton("Blau");
		btnschriftblau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lbltextaendern.setForeground(Color.BLUE);
			}
		});
		btnschriftblau.setBounds(129, 312, 113, 23);
		formaendernPane.add(btnschriftblau);
		
		JButton btnschriftschwarz = new JButton("Schwarz");
		btnschriftschwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lbltextaendern.setForeground(Color.BLACK);
			}
		});
		btnschriftschwarz.setBounds(248, 312, 113, 23);
		formaendernPane.add(btnschriftschwarz);
		
		JButton btnplus = new JButton("+");
		btnplus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font f = lbltextaendern.getFont();
				lbltextaendern.setFont(f.deriveFont(f.getStyle(), f.getSize() + 1));
			}
		});
		btnplus.setBounds(10, 367, 170, 23);
		formaendernPane.add(btnplus);
		
		JButton btnminus = new JButton("-");
		btnminus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Font f = lbltextaendern.getFont();
				lbltextaendern.setFont(f.deriveFont(f.getStyle(), f.getSize() - 1));
			}
		});
		btnminus.setBounds(190, 367, 170, 23);
		formaendernPane.add(btnminus);
		
		JButton btnleft = new JButton("linksb\u00FCndig");
		btnleft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lbltextaendern.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnleft.setBounds(10, 426, 113, 23);
		formaendernPane.add(btnleft);
		
		JButton btncenter = new JButton("zentriert");
		btncenter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lbltextaendern.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btncenter.setBounds(128, 426, 113, 23);
		formaendernPane.add(btncenter);
		
		JButton btnright = new JButton("rechtsb\u00FCndig");
		btnright.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lbltextaendern.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnright.setBounds(247, 426, 113, 23);
		formaendernPane.add(btnright);
		
		JLabel lblAufgabe6 = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabe6.setBounds(9, 469, 351, 14);
		formaendernPane.add(lblAufgabe6);
		
		JLabel lblAufgabe1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabe1.setBounds(10, 95, 350, 14);
		formaendernPane.add(lblAufgabe1);
		
		JLabel lblAufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lblAufgabe2.setBounds(10, 187, 350, 14);
		formaendernPane.add(lblAufgabe2);
		
		JLabel lblAufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabe3.setBounds(9, 295, 351, 14);
		formaendernPane.add(lblAufgabe3);
		
		JLabel lblAufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblAufgabe4.setBounds(10, 346, 350, 14);
		formaendernPane.add(lblAufgabe4);
		
		JLabel lblAufgabe5 = new JLabel("Aufgbe 5: Textausrichtung");
		lblAufgabe5.setBounds(10, 401, 350, 14);
		formaendernPane.add(lblAufgabe5);
		
		lbltextaendern = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lbltextaendern.setHorizontalAlignment(SwingConstants.CENTER);
		lbltextaendern.setBounds(10, 11, 350, 67);
		formaendernPane.add(lbltextaendern);
			
		}	
}
